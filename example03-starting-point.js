// This is a completely minimal Express / Handlebars application which sets everything up, and allows for "public" files to be served.

var express = require('express');
var fs = require('fs');

var app = express();

app.set('port', process.env.PORT || 3000);

var handlebars = require('express-handlebars');
app.engine('handlebars', handlebars({ defaultLayout: 'main' }));
app.set('view engine', 'handlebars');

app.use(express.static(__dirname + "/public"));

app.listen(app.get('port'), function () {
    console.log('Express started on http://localhost:' + app.get('port'));
});
