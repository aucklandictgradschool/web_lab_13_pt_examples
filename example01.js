
// Allow us to use the Express framework
var express = require('express');

// Setup a new Express app
var app = express();

// The app should listen on port 3000, unless a different
// port is specified in the environment.
app.set('port', process.env.PORT || 3000);

// Specify that the app should use handlebars, and set handlebars as the app's view engine.
var handlebars = require('express-handlebars');
app.engine('handlebars', handlebars({ defaultLayout: 'main' }));
app.set('view engine', 'handlebars');

// Specify that when we browse to "/" with a GET request, render home.handlebars.
app.get('/', function (req, res) {

    // Data to send through to the "home" page
    var data = {
        layout: false, // A special property which will force handlebars to not use a Layout (see later examples).
        name: "Thomas",
        phoneNumber: "+00 0 123 4567",
        someEscapedData: "<strong>Strong text!</strong>",
        someUnescapedData: "<strong>Strong text!</strong>",
        email: "thomasTheTRex@notexist.org"
    };

    // Pass the data as the second argument to send stuff through.
    res.render('example01/home', data);
});

/* We haven't specified any other routes, so browsing anywhere other than "/" will result in a default error page being returned. */

// Start the server running.
app.listen(app.get('port'), function () {
    console.log('Express started on http://localhost:' + app.get('port'));
});